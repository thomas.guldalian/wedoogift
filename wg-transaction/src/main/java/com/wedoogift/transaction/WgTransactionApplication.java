package com.wedoogift.transaction;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

import com.wedoogift.core.DefaultProfileUtil;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class, DataSourceTransactionManagerAutoConfiguration.class,
    HibernateJpaAutoConfiguration.class})
@EnableFeignClients(basePackages = {"com.wedoogift"})
@ComponentScan(basePackages = {"com.wedoogift"})
public class WgTransactionApplication {

  public static void main(String[] args) {
    SpringApplication app = new SpringApplication(WgTransactionApplication.class);
    DefaultProfileUtil.addDefaultProfile(app);
    app.run(args).getEnvironment();
  }
}
