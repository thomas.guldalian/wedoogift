package com.wedoogift.transaction.service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.EnumUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.wdoogift.deposite.api.dto.BalanceEmployeeDto;
import com.wdoogift.deposite.api.utils.DepositeTypeEnum;
import com.wdoogift.deposite.api.utils.OriginTypeEnum;
import com.wedoogift.core.constants.ErrorConstants;
import com.wedoogift.core.exception.WedoogiftValidationException;
import com.wedoogift.core.utils.DateUtils;
import com.wedoogift.population.api.dto.CompanyDto;
import com.wedoogift.population.api.dto.EmployeeDto;
import com.wedoogift.population.api.rest.WgPopulationApi;
import com.wedoogift.transaction.dao.CompanyBalanceDao;
import com.wedoogift.transaction.dao.TransactionDao;
import com.wedoogift.transaction.entity.CompanyBalance;
import com.wedoogift.transaction.entity.Deposite;
import com.wedoogift.transaction.entity.Transaction;
import com.wedoogift.transaction.factory.DepositeFactory;
import com.wedoogift.transaction.mapper.DepositeMapper;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class TransactionService {

  private final TransactionDao dao;

  private final CompanyBalanceDao companyBalanceDao;

  private final DepositeMapper depositeMapper;

  private final DepositeFactory depositeFactory;

  private final WgPopulationApi populationApi;

  public boolean addBalanceToCompany(String codeCompany, String origin, BigDecimal value) {
    Date dateExecution = new Date();

    //check existing company
    CompanyDto company = populationApi.findCompanyByCode(codeCompany);
    if (null == company) {
      throw new WedoogiftValidationException(BalanceEmployeeDto.class,
              Map.of(ErrorConstants.UNKNOW_ENTITY_KEY, String.format(ErrorConstants.UNKNOW_ENTITY_LIB, "Company")));
    }

    CompanyBalance companyBalance = companyBalanceDao.findById(codeCompany).orElse(null);
    if (null == companyBalance) {
      companyBalance = buildCompanyBalance(codeCompany);
    }
    Transaction transaction = buildTransaction(codeCompany, origin, OriginTypeEnum.IN, value, dateExecution);
    companyBalance.setBalance(companyBalance.getBalance().add(value));
    return dao.saveInCompnyBalance(companyBalance, transaction);
  }

  public boolean depositeToEmployee(String codeCompany, String codeEmployee, String type, BigDecimal value) {
    Date dateDeposite = new Date();
    //check existing company
    CompanyDto company = populationApi.findCompanyByCode(codeCompany);
    if (null == company) {
      throw new WedoogiftValidationException(BalanceEmployeeDto.class,
              Map.of(ErrorConstants.UNKNOW_ENTITY_KEY, String.format(ErrorConstants.UNKNOW_ENTITY_LIB, "Company")));
    }
    //check existing company
    EmployeeDto employee = populationApi.findEmployeeByCode(codeEmployee);
    if (null == employee) {
      throw new WedoogiftValidationException(BalanceEmployeeDto.class,
              Map.of(ErrorConstants.UNKNOW_ENTITY_KEY, String.format(ErrorConstants.UNKNOW_ENTITY_LIB, "Employee")));
    }

    if (!StringUtils.equals(codeCompany, employee.getCompany())) {
      throw new WedoogiftValidationException(BalanceEmployeeDto.class,
              Map.of(ErrorConstants.NO_MATCH_COMPANY_EMPLOYEE_KEY, String.format(ErrorConstants.NO_MATCH_COMPANY_EMPLOYEE_LIB, codeCompany)));
    }

    if (!EnumUtils.isValidEnum(DepositeTypeEnum.class, type)) {
      throw new IllegalArgumentException(String.format(ErrorConstants.INVALID_ENUM_ERROR, "Type", DepositeTypeEnum.values()));
    }

    CompanyBalance companyBalance = companyBalanceDao.findById(codeCompany).orElse(null);

    if (null == companyBalance || value.compareTo(companyBalance.getBalance()) > 0) {
      throw new WedoogiftValidationException(BalanceEmployeeDto.class, Map.of(ErrorConstants.NOT_ENOUGH_BALANCE_KEY,
              String.format(ErrorConstants.NOT_ENOUGH_BALANCE_LIB, (null == companyBalance) ? BigDecimal.ZERO : companyBalance.getBalance())));
    }

    Deposite deposite = buildDeposite(EnumUtils.getEnum(DepositeTypeEnum.class, type), codeEmployee, codeCompany, value, dateDeposite);
    Transaction transaction = buildTransaction(codeCompany, codeEmployee, OriginTypeEnum.OUT, value, dateDeposite);
    companyBalance.setBalance(companyBalance.getBalance().subtract(value));

    dao.saveDeposite(deposite, companyBalance, transaction);

    return true;

  }

  public BalanceEmployeeDto getBalanceEmployee(String codeEmployee, LocalDate dateSearch) {

    EmployeeDto employee = populationApi.findEmployeeByCode(codeEmployee);
    if (null == employee) {
      throw new WedoogiftValidationException(BalanceEmployeeDto.class,
              Map.of(ErrorConstants.UNKNOW_ENTITY_KEY, String.format(ErrorConstants.UNKNOW_ENTITY_LIB, "Employee")));
    }

    List<Deposite> deposites = dao.getDepositesOfEmployee(codeEmployee);

    BalanceEmployeeDto result = new BalanceEmployeeDto();
    result.setCodeEmployee(codeEmployee);
    if (CollectionUtils.isEmpty(deposites)) {
      result.setBalance(BigDecimal.ZERO);
      return result;
    }

    Date date = DateUtils.getDateFromLocal(dateSearch);

    List<Deposite> filtredDeposite = deposites.stream().filter(
            deposite -> deposite.isActive() && deposite.getStartDeposite().compareTo(date) <= 0 && deposite.getEndDeposite().compareTo(date) >= 0)
            .collect(Collectors.toList());
    BigDecimal balance = filtredDeposite.stream().map(Deposite::getPrize).reduce(new BigDecimal(0), BigDecimal::add);

    result.setBalance(balance);
    result.setDeposites(depositeMapper.toDtos(filtredDeposite));

    return result;
  }

  private Deposite buildDeposite(DepositeTypeEnum type, String codeEmployee, String codeCompany, BigDecimal prize, Date start) {
    return depositeFactory.getBuilder(type).build(codeEmployee, codeCompany, prize, start);
  }

  private CompanyBalance buildCompanyBalance(String codeCompany) {
    CompanyBalance companyBalance = new CompanyBalance();
    companyBalance.setCodeCompany(codeCompany);
    companyBalance.setBalance(BigDecimal.ZERO);
    return companyBalance;
  }

  private Transaction buildTransaction(String codeCompany, String origin, OriginTypeEnum type, BigDecimal value, Date dateTransaction) {
    Transaction transaction = new Transaction();
    transaction.setCodeCompany(codeCompany);
    transaction.setOrigin(origin);
    transaction.setType(type);
    transaction.setValue(value);
    transaction.setTransactionDate(dateTransaction);
    return transaction;
  }
}
