package com.wedoogift.transaction.entity;

import java.math.BigDecimal;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBVersionAttribute;

import lombok.Setter;

@Setter
@DynamoDBTable(tableName = "company-balance")
public class CompanyBalance {

  private String codeCompany;

  private BigDecimal balance;

  private Long version;

  @DynamoDBHashKey(attributeName = "codeCompany")
  public String getCodeCompany() {
    return codeCompany;
  }

  @DynamoDBAttribute(attributeName = "balance")
  public BigDecimal getBalance() {
    return balance;
  }

  @DynamoDBVersionAttribute
  public Long getVersion() {
    return version;
  }
}
