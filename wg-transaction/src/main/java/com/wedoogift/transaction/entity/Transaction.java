package com.wedoogift.transaction.entity;

import java.math.BigDecimal;
import java.util.Date;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAutoGeneratedKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConvertedEnum;
import com.wdoogift.deposite.api.utils.OriginTypeEnum;

import lombok.Setter;
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbSecondaryPartitionKey;

@Setter
@DynamoDBTable(tableName = "transaction")
public class Transaction {

  private String id;

  private String codeCompany;

  private String origin;

  private OriginTypeEnum type;

  private BigDecimal value;

  private Date transactionDate;

  @DynamoDBHashKey(attributeName = "Id")
  @DynamoDBAutoGeneratedKey
  public String getId() {
    return id;
  }

  @DynamoDbSecondaryPartitionKey(indexNames = "transaction_by_company")
  @DynamoDBAttribute
  public String getCodeCompany() {
    return codeCompany;
  }

  @DynamoDBAttribute(attributeName = "origin")
  public String getOrigin() {
    return origin;
  }

  @DynamoDBTypeConvertedEnum
  @DynamoDBAttribute(attributeName = "type")
  public OriginTypeEnum getType() {
    return type;
  }

  @DynamoDBAttribute(attributeName = "value")
  public BigDecimal getValue() {
    return value;
  }

  @DynamoDBAttribute(attributeName = "transactionDate")
  public Date getTransactionDate() {
    return transactionDate;
  }
}
