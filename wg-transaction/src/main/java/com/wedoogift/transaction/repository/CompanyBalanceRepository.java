package com.wedoogift.transaction.repository;

import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

import com.wedoogift.transaction.entity.CompanyBalance;

@EnableScan
public interface CompanyBalanceRepository extends CrudRepository<CompanyBalance, String> {

  CompanyBalance findByCodeCompany(String codeCompany);
}
