package com.wedoogift.transaction.repository;

import java.util.List;

import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

import com.wedoogift.transaction.entity.Transaction;

@EnableScan
public interface TransactionRepository extends CrudRepository<Transaction, String> {

  List<Transaction> findByCodeCompany(String codeCompany);
}
