package com.wedoogift.transaction.repository;

import java.util.List;

import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

import com.wedoogift.transaction.entity.Deposite;

@EnableScan
public interface DepositeRepository extends CrudRepository<Deposite, String> {

  List<Deposite> findByCodeEmployee(String codeEmployee);
}
