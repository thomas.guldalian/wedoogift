package com.wedoogift.transaction.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import com.wdoogift.deposite.api.dto.DepositeDto;
import com.wedoogift.transaction.entity.Deposite;

@Mapper(componentModel = "spring")
public interface DepositeMapper {

  DepositeDto toDto(Deposite entity);

  Deposite toEntity(DepositeDto dto);

  List<DepositeDto> toDtos(List<Deposite> entities);

  List<Deposite> toEntities(List<DepositeDto> dtos);
}
