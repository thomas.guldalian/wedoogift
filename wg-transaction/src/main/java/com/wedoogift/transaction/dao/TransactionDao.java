package com.wedoogift.transaction.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMappingException;
import com.amazonaws.services.dynamodbv2.datamodeling.TransactionWriteRequest;
import com.amazonaws.services.dynamodbv2.model.InternalServerErrorException;
import com.amazonaws.services.dynamodbv2.model.ResourceNotFoundException;
import com.amazonaws.services.dynamodbv2.model.TransactionCanceledException;
import com.wedoogift.transaction.entity.CompanyBalance;
import com.wedoogift.transaction.entity.Deposite;
import com.wedoogift.transaction.entity.Transaction;
import com.wedoogift.transaction.repository.DepositeRepository;
import com.wedoogift.transaction.repository.TransactionRepository;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Repository
@AllArgsConstructor
@Slf4j
public class TransactionDao {

  private final AmazonDynamoDB client;

  private final DynamoDBMapper mapper;

  private final DepositeRepository depositeRepository;

  private final TransactionRepository transactionRepository;

  public boolean saveDeposite(Deposite deposite, CompanyBalance companyBalance, Transaction transaction) {

    TransactionWriteRequest transactionWriteRequest = new TransactionWriteRequest();
    transactionWriteRequest.addPut(deposite);
    transactionWriteRequest.addPut(companyBalance);
    transactionWriteRequest.addPut(transaction);
    return executeTransactionWrite(transactionWriteRequest);
  }

  public boolean saveInCompnyBalance(CompanyBalance companyBalance, Transaction transaction) {

    TransactionWriteRequest transactionWriteRequest = new TransactionWriteRequest();
    transactionWriteRequest.addPut(companyBalance);
    transactionWriteRequest.addPut(transaction);
    return executeTransactionWrite(transactionWriteRequest);
  }

  public List<Deposite> findByCodeEmployee(String codeEmployee) {
    return depositeRepository.findByCodeEmployee(codeEmployee);
  }

  public List<Deposite> getDepositesOfEmployee(String codeEmployee) {
    return depositeRepository.findByCodeEmployee(codeEmployee);
  }

  private boolean executeTransactionWrite(TransactionWriteRequest transactionWriteRequest) {
    boolean success = false;
    try {
      mapper.transactionWrite(transactionWriteRequest);
      success = true;

    } catch (DynamoDBMappingException ddbme) {
      log.error("Client side error in Mapper, fix before retrying. Error: " + ddbme.getMessage());
    } catch (ResourceNotFoundException rnfe) {
      log.error("One of the tables was not found, verify table exists before retrying. Error: " + rnfe.getMessage());
    } catch (InternalServerErrorException ise) {
      log.error("Internal Server Error, generally safe to retry with back-off. Error: " + ise.getMessage());
    } catch (TransactionCanceledException tce) {
      log.error("Transaction Canceled, implies a client issue, fix before retrying. Error: " + tce.getMessage());
    } catch (Exception ex) {
      log.error("An exception occurred, investigate and configure retry strategy. Error: " + ex.getMessage());
    }
    return success;
  }

}
