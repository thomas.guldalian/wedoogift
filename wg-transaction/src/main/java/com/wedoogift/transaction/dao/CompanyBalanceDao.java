package com.wedoogift.transaction.dao;

import java.util.Optional;

import org.springframework.stereotype.Repository;

import com.wedoogift.transaction.entity.CompanyBalance;
import com.wedoogift.transaction.repository.CompanyBalanceRepository;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@Repository
public class CompanyBalanceDao {

  private final CompanyBalanceRepository repository;

  public Optional<CompanyBalance> findById(String codeCompany) {
    return repository.findById(codeCompany);
  }
}
