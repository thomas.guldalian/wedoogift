package com.wedoogift.transaction.resource;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wdoogift.deposite.api.dto.BalanceEmployeeDto;
import com.wedoogift.core.utils.UrlUtils;
import com.wedoogift.transaction.service.TransactionService;

@RestController
@RequestMapping(value = UrlUtils.TRANSACTION_URL)
public class TransactionResource {

  private final TransactionService service;

  public TransactionResource(TransactionService service) {
    this.service = service;
  }

  @GetMapping
  public boolean addBalanceToCompany(@RequestParam String codeCompany, @RequestParam String origin, @RequestParam BigDecimal value) {
    return service.addBalanceToCompany(codeCompany, origin, value);
  }

  @GetMapping("deposite")
  public boolean depositeToEmployee(@RequestParam String codeCompany, @RequestParam String codeEmployee, @RequestParam String type,
          @RequestParam BigDecimal value) {
    return service.depositeToEmployee(codeCompany, codeEmployee, type, value);
  }

  @GetMapping("balance-employee")
  public BalanceEmployeeDto getBalanceEmployee(@RequestParam String codeEmployee,
          @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate dateSearch) {
    return service.getBalanceEmployee(codeEmployee, dateSearch);
  }
}
