package com.wedoogift.transaction.factory.builder;

import java.util.Calendar;
import java.util.Date;

import org.springframework.stereotype.Component;

import com.wdoogift.deposite.api.utils.DepositeTypeEnum;
import com.wedoogift.core.utils.DateUtils;

@Component
public class GiftDepositeBuilder implements Builder {

  private static final DepositeTypeEnum DEPOSITE_TYPE = DepositeTypeEnum.GIFT;

  @Override
  public DepositeTypeEnum getType() {
    return DEPOSITE_TYPE;
  }

  @Override
  public Date getEndDeposite(Date start) {
    Calendar calendar = DateUtils.getCalendar(start);
    return DateUtils.addUnitsToDate(Calendar.YEAR, 1, calendar.getTime());
  }
}
