package com.wedoogift.transaction.factory;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wdoogift.deposite.api.utils.DepositeTypeEnum;
import com.wedoogift.transaction.factory.builder.Builder;

@Component
public class DepositeFactory {

  private Map<DepositeTypeEnum, Builder> builderMap;

  @Autowired
  private DepositeFactory(List<Builder> builders) {
    builderMap = builders.stream().collect(Collectors.toUnmodifiableMap(Builder::getType, Function.identity()));
  }

  public Builder getBuilder(DepositeTypeEnum depositeType) {
    return Optional.ofNullable(builderMap.get(depositeType)).orElseThrow(IllegalArgumentException::new);
  }
}
