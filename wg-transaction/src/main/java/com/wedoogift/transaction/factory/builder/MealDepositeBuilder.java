package com.wedoogift.transaction.factory.builder;

import java.util.Calendar;
import java.util.Date;

import org.springframework.stereotype.Component;

import com.wdoogift.deposite.api.utils.DepositeTypeEnum;
import com.wedoogift.core.utils.DateUtils;

@Component
public class MealDepositeBuilder implements Builder {

  private static final DepositeTypeEnum DEPOSITE_TYPE = DepositeTypeEnum.MEAL;

  @Override
  public DepositeTypeEnum getType() {
    return DEPOSITE_TYPE;
  }

  @Override
  public Date getEndDeposite(Date start) {
    Calendar calendar = DateUtils.getCalendar(start);
    int year = calendar.get(Calendar.YEAR);

    Date endDate = DateUtils.getLastDayOfMonth(2, year);
    if (endDate.compareTo(calendar.getTime()) < 0) {
      endDate = DateUtils.getLastDayOfMonth(2, year + 1);
    }
    return endDate;
  }

}
