package com.wedoogift.transaction.factory.builder;

import java.math.BigDecimal;
import java.util.Date;

import com.wdoogift.deposite.api.utils.DepositeTypeEnum;
import com.wedoogift.transaction.entity.Deposite;

public interface Builder {

  DepositeTypeEnum getType();

  default Deposite build(String codeEmployee, String codeCompany, BigDecimal prize, Date start) {
    return Deposite.builder().codeCompany(codeCompany).codeEmployee(codeEmployee).active(true).giftType(getType()).prize(prize).startDeposite(start)
            .endDeposite(getEndDeposite(start)).build();
  }

  Date getEndDeposite(Date start);

}
