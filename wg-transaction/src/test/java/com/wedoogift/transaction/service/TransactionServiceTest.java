package com.wedoogift.transaction.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.hamcrest.MatcherAssert;
import org.hamcrest.collection.IsMapContaining;
import org.hamcrest.collection.IsMapWithSize;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import com.wdoogift.deposite.api.dto.BalanceEmployeeDto;
import com.wdoogift.deposite.api.utils.DepositeTypeEnum;
import com.wedoogift.core.constants.ErrorConstants;
import com.wedoogift.core.exception.WedoogiftValidationException;
import com.wedoogift.core.utils.DateUtils;
import com.wedoogift.population.api.dto.CompanyDto;
import com.wedoogift.population.api.dto.EmployeeDto;
import com.wedoogift.population.api.rest.WgPopulationApi;
import com.wedoogift.transaction.dao.CompanyBalanceDao;
import com.wedoogift.transaction.dao.TransactionDao;
import com.wedoogift.transaction.entity.CompanyBalance;
import com.wedoogift.transaction.entity.Deposite;
import com.wedoogift.transaction.factory.DepositeFactory;
import com.wedoogift.transaction.factory.builder.GiftDepositeBuilder;
import com.wedoogift.transaction.mapper.DepositeMapper;

@ExtendWith(MockitoExtension.class)
class TransactionServiceTest {

  @InjectMocks
  private TransactionService service;

  @Mock
  private TransactionDao dao;

  @Mock
  private CompanyBalanceDao companyBalanceDao;

  @Spy
  private DepositeMapper depositeMapper = Mappers.getMapper(DepositeMapper.class);

  @Mock
  private DepositeFactory depositeFactory;

  @Mock
  private GiftDepositeBuilder builder;

  @Mock
  private WgPopulationApi populationApi;

  private String codeCompany;

  private String codeEmployee;

  private String type;

  private BigDecimal value;

  private CompanyDto company;

  private EmployeeDto employee;

  private CompanyBalance companyBalance;

  private Deposite deposite;

  private Date date;

  @BeforeEach
  void SetUp() {
    date = DateUtils.getCalendarZonedFromYearMonthDay(2022, 6, 1).getTime();

    codeCompany = "COMPANY";
    codeEmployee = "123456";
    type = "GIFT";
    value = BigDecimal.valueOf(35l);

    company = new CompanyDto();
    company.setCode(codeCompany);
    company.setLibelle("libelle company");
    company.setCreatedDate(date);
    company.setModifiedDate(date);
    company.setVersion(0l);

    employee = new EmployeeDto();
    employee.setCode(codeEmployee);
    employee.setCompany(codeCompany);
    employee.setMatricule("65428");
    employee.setName("dupont");
    employee.setSurname("jean");
    employee.setCreatedDate(date);
    employee.setModifiedDate(date);
    employee.setVersion(0l);

    companyBalance = new CompanyBalance();
    companyBalance.setCodeCompany(codeCompany);
    companyBalance.setBalance(BigDecimal.valueOf(156l));
    companyBalance.setVersion(0l);

    deposite = Deposite.builder().codeEmployee(codeEmployee).codeCompany(codeCompany).startDeposite(date).endDeposite(date)
            .giftType(DepositeTypeEnum.GIFT).prize(value).active(true).version(0l).build();
  }

  @Test
  void testAddBalanceToCompany_ok() {

    String origin = "TEST";

    when(populationApi.findCompanyByCode(codeCompany)).thenReturn(company);
    when(companyBalanceDao.findById(codeCompany)).thenReturn(Optional.of(companyBalance));

    when(dao.saveInCompnyBalance(Mockito.eq(companyBalance), any())).thenReturn(true);

    boolean result = service.addBalanceToCompany(codeCompany, origin, value);
    assertTrue(result);

  }

  @Test
  void testAddBalanceToCompany_ko_company() {

    String codeCompany = "COMPANY";
    String origin = "TEST";
    BigDecimal value = BigDecimal.valueOf(1452l);

    when(populationApi.findCompanyByCode(codeCompany)).thenReturn(null);

    WedoogiftValidationException exception = Assertions.assertThrows(WedoogiftValidationException.class, () -> {
      service.addBalanceToCompany(codeCompany, origin, value);
    });

    assertWedoogiftValidationExceptionCode(exception, ErrorConstants.UNKNOW_ENTITY_KEY);

  }

  @Test
  void testAddBalanceToCompany_ok_build() {

    Date date = new Date();

    String codeCompany = "COMPANY";
    String origin = "TEST";
    BigDecimal value = BigDecimal.valueOf(1452l);

    when(populationApi.findCompanyByCode(codeCompany)).thenReturn(company);
    when(companyBalanceDao.findById(codeCompany)).thenReturn(Optional.ofNullable(null));

    when(dao.saveInCompnyBalance(any(), any())).thenReturn(true);

    boolean result = service.addBalanceToCompany(codeCompany, origin, value);
    assertTrue(result);

  }

  @Test
  void testDepositeToEmployee_ok() {

    when(populationApi.findCompanyByCode(codeCompany)).thenReturn(company);
    when(populationApi.findEmployeeByCode(codeEmployee)).thenReturn(employee);
    when(companyBalanceDao.findById(codeCompany)).thenReturn(Optional.of(companyBalance));
    when(depositeFactory.getBuilder(any())).thenReturn(builder);
    when(builder.build(any(), any(), any(), any())).thenReturn(deposite);
    when(dao.saveDeposite(any(), any(), any())).thenReturn(true);

    boolean result = service.depositeToEmployee(codeCompany, codeEmployee, type, value);
    assertTrue(result);
  }

  @Test
  void testDepositeToEmployee_ko_company() {

    when(populationApi.findCompanyByCode(codeCompany)).thenReturn(null);

    WedoogiftValidationException exception = Assertions.assertThrows(WedoogiftValidationException.class, () -> {
      service.depositeToEmployee(codeCompany, codeEmployee, type, value);
    });

    assertWedoogiftValidationExceptionCode(exception, ErrorConstants.UNKNOW_ENTITY_KEY);
  }

  @Test
  void testDepositeToEmployee_ko_employee() {

    when(populationApi.findCompanyByCode(codeCompany)).thenReturn(company);
    when(populationApi.findEmployeeByCode(codeEmployee)).thenReturn(null);

    WedoogiftValidationException exception = Assertions.assertThrows(WedoogiftValidationException.class, () -> {
      service.depositeToEmployee(codeCompany, codeEmployee, type, value);
    });

    assertWedoogiftValidationExceptionCode(exception, ErrorConstants.UNKNOW_ENTITY_KEY);
  }

  @Test
  void testDepositeToEmployee_ko_match_company() {

    EmployeeDto employee = new EmployeeDto();
    employee.setCode(codeEmployee);
    employee.setCompany("autre_company");
    employee.setMatricule("65428");
    employee.setName("dupont");
    employee.setSurname("jean");
    employee.setCreatedDate(date);
    employee.setModifiedDate(date);
    employee.setVersion(0l);

    when(populationApi.findCompanyByCode(codeCompany)).thenReturn(company);
    when(populationApi.findEmployeeByCode(codeEmployee)).thenReturn(employee);

    WedoogiftValidationException exception = Assertions.assertThrows(WedoogiftValidationException.class, () -> {
      service.depositeToEmployee(codeCompany, codeEmployee, type, value);
    });

    assertWedoogiftValidationExceptionCode(exception, ErrorConstants.NO_MATCH_COMPANY_EMPLOYEE_KEY);
  }

  @Test
  void testDepositeToEmployee_ko_type_deposite() {
    String type = "DEPOSITE";

    when(populationApi.findCompanyByCode(codeCompany)).thenReturn(company);
    when(populationApi.findEmployeeByCode(codeEmployee)).thenReturn(employee);

    Assertions.assertThrows(IllegalArgumentException.class, () -> {
      service.depositeToEmployee(codeCompany, codeEmployee, type, value);
    });
  }

  @Test
  void testDepositeToEmployee_ko_balance_null() {

    when(populationApi.findCompanyByCode(codeCompany)).thenReturn(company);
    when(populationApi.findEmployeeByCode(codeEmployee)).thenReturn(employee);
    when(companyBalanceDao.findById(codeCompany)).thenReturn(Optional.ofNullable(null));

    WedoogiftValidationException exception = Assertions.assertThrows(WedoogiftValidationException.class, () -> {
      service.depositeToEmployee(codeCompany, codeEmployee, type, value);
    });

    assertWedoogiftValidationExceptionCode(exception, ErrorConstants.NOT_ENOUGH_BALANCE_KEY);
  }

  @Test
  void testDepositeToEmployee_ko_balance_not_enough() {

    CompanyBalance companyBalance = new CompanyBalance();
    companyBalance.setCodeCompany(codeCompany);
    companyBalance.setBalance(BigDecimal.valueOf(25l));
    companyBalance.setVersion(0l);

    when(populationApi.findCompanyByCode(codeCompany)).thenReturn(company);
    when(populationApi.findEmployeeByCode(codeEmployee)).thenReturn(employee);
    when(companyBalanceDao.findById(codeCompany)).thenReturn(Optional.of(companyBalance));

    WedoogiftValidationException exception = Assertions.assertThrows(WedoogiftValidationException.class, () -> {
      service.depositeToEmployee(codeCompany, codeEmployee, type, value);
    });

    assertWedoogiftValidationExceptionCode(exception, ErrorConstants.NOT_ENOUGH_BALANCE_KEY);
  }

  @Test
  void TestGetBalanceEmployee() {
    LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    when(populationApi.findEmployeeByCode(any())).thenReturn(employee);
    when(dao.getDepositesOfEmployee(any())).thenReturn(List.of(deposite));

    BalanceEmployeeDto balanceEmployee = service.getBalanceEmployee(codeEmployee, localDate);
    assertEquals(deposite.getPrize(), balanceEmployee.getBalance());
    assertEquals(1, balanceEmployee.getDeposites().size());
    assertEquals(codeEmployee, balanceEmployee.getCodeEmployee());

  }

  @Test
  void TestGetBalanceEmployee_no_deposite() {

    LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    when(populationApi.findEmployeeByCode(any())).thenReturn(employee);
    when(dao.getDepositesOfEmployee(any())).thenReturn(new ArrayList<>());

    BalanceEmployeeDto balanceEmployee = service.getBalanceEmployee(codeEmployee, localDate);
    assertEquals(BigDecimal.ZERO, balanceEmployee.getBalance());
    assertNull(balanceEmployee.getDeposites());
    assertEquals(codeEmployee, balanceEmployee.getCodeEmployee());

  }

  @Test
  void TestGetBalanceEmployee_ko_employee() {

    LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    when(populationApi.findEmployeeByCode(any())).thenReturn(null);

    WedoogiftValidationException exception = Assertions.assertThrows(WedoogiftValidationException.class, () -> {
      service.getBalanceEmployee(codeEmployee, localDate);
    });

    assertWedoogiftValidationExceptionCode(exception, ErrorConstants.UNKNOW_ENTITY_KEY);

  }

  @Test
  void TestGetBalanceEmployee_zero() {
    Date startDeposite = DateUtils.getCalendarZonedFromYearMonthDay(2021, 5, 31).getTime();
    Date endDeposite = DateUtils.getCalendarZonedFromYearMonthDay(2022, 5, 31).getTime();

    deposite = Deposite.builder().codeEmployee(codeEmployee).codeCompany(codeCompany).startDeposite(startDeposite).endDeposite(endDeposite)
            .giftType(DepositeTypeEnum.GIFT).prize(value).active(true).version(0l).build();

    LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    when(populationApi.findEmployeeByCode(any())).thenReturn(employee);
    when(dao.getDepositesOfEmployee(any())).thenReturn(List.of(deposite));

    BalanceEmployeeDto balanceEmployee = service.getBalanceEmployee(codeEmployee, localDate);
    assertEquals(BigDecimal.ZERO, balanceEmployee.getBalance());
    assertEquals(0, balanceEmployee.getDeposites().size());
    assertEquals(codeEmployee, balanceEmployee.getCodeEmployee());

  }

  private void assertWedoogiftValidationExceptionCode(WedoogiftValidationException exception, String code) {
    Assertions.assertNotNull(exception);
    Assertions.assertNotNull(exception.getValidationErrors());
    MatcherAssert.assertThat(exception.getValidationErrors(), IsMapWithSize.aMapWithSize(1));
    MatcherAssert.assertThat(exception.getValidationErrors(), IsMapContaining.hasKey(code));
  }

}