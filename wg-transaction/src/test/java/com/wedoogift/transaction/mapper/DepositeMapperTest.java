package com.wedoogift.transaction.mapper;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.junit.jupiter.MockitoExtension;

import com.wdoogift.deposite.api.dto.DepositeDto;
import com.wdoogift.deposite.api.utils.DepositeTypeEnum;
import com.wedoogift.core.utils.DateUtils;
import com.wedoogift.transaction.entity.Deposite;

@ExtendWith(MockitoExtension.class)
class DepositeMapperTest {

  private final DepositeMapper mapper = Mappers.getMapper(DepositeMapper.class);

  private Deposite entity;

  private DepositeDto dto;

  @BeforeEach
  void init() {
    Date startDate = DateUtils.getCalendarZonedFromYearMonthDay(2022, 6, 8).getTime();
    Date endDate = DateUtils.getCalendarZonedFromYearMonthDay(2023, 6, 8).getTime();

    entity = Deposite.builder().id("123e4567-e89b-12d3-a456-426614174000").codeCompany("COMPANY").codeEmployee("1111").active(true)
            .startDeposite(startDate).endDeposite(endDate).giftType(DepositeTypeEnum.GIFT).prize(BigDecimal.valueOf(152)).version(0l).build();

    dto = new DepositeDto();

    dto.setId("00000000-0000-0000-0000-000000000000");
    dto.setCodeCompany("COMPANY2");
    dto.setCodeEmployee("2222");
    dto.setActive(false);
    dto.setStartDeposite(startDate);
    dto.setEndDeposite(endDate);
    dto.setGiftType(DepositeTypeEnum.MEAL);
    dto.setPrize(BigDecimal.valueOf(496156));
    dto.setVersion(25l);
  }

  @Test
  void toDto() {

    DepositeDto result = mapper.toDto(entity);
    assertThat(result).hasFieldOrPropertyWithValue("id", entity.getId()).hasFieldOrPropertyWithValue("codeCompany", entity.getCodeCompany())
            .hasFieldOrPropertyWithValue("codeEmployee", entity.getCodeEmployee()).hasFieldOrPropertyWithValue("active", entity.isActive())
            .hasFieldOrPropertyWithValue("startDeposite", entity.getStartDeposite())
            .hasFieldOrPropertyWithValue("endDeposite", entity.getEndDeposite()).hasFieldOrPropertyWithValue("giftType", entity.getGiftType())
            .hasFieldOrPropertyWithValue("prize", entity.getPrize()).hasFieldOrPropertyWithValue("version", entity.getVersion());
  }

  @Test
  void toEntity() {

    Deposite result = mapper.toEntity(dto);
    assertThat(result).hasFieldOrPropertyWithValue("id", dto.getId()).hasFieldOrPropertyWithValue("codeCompany", dto.getCodeCompany())
            .hasFieldOrPropertyWithValue("codeEmployee", dto.getCodeEmployee()).hasFieldOrPropertyWithValue("active", dto.isActive())
            .hasFieldOrPropertyWithValue("startDeposite", dto.getStartDeposite()).hasFieldOrPropertyWithValue("endDeposite", dto.getEndDeposite())
            .hasFieldOrPropertyWithValue("giftType", dto.getGiftType()).hasFieldOrPropertyWithValue("prize", dto.getPrize())
            .hasFieldOrPropertyWithValue("version", dto.getVersion());
  }

  @Test
  void toDtos() {
    List<Deposite> entities = Arrays.asList(entity);
    List<DepositeDto> dtos = mapper.toDtos(entities);

    Assertions.assertEquals(1, dtos.size());
  }

  @Test
  void toEntities() {
    List<DepositeDto> dtos = Arrays.asList(dto);
    List<Deposite> entities = mapper.toEntities(dtos);

    Assertions.assertEquals(1, entities.size());
  }
}