package com.wdoogift.deposite.api.utils;

public enum DepositeTypeEnum {
  GIFT, MEAL;
}
