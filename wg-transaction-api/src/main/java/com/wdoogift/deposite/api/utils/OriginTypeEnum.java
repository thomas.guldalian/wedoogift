package com.wdoogift.deposite.api.utils;

public enum OriginTypeEnum {

  IN, OUT;
}
