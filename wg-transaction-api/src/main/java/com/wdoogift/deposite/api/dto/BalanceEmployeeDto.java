package com.wdoogift.deposite.api.dto;

import java.math.BigDecimal;
import java.util.List;

import lombok.Data;

@Data
public class BalanceEmployeeDto {

  private String codeEmployee;

  private BigDecimal balance;

  private List<DepositeDto> deposites;
}
