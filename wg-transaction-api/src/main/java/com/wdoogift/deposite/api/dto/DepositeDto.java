package com.wdoogift.deposite.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.wdoogift.deposite.api.utils.DepositeTypeEnum;

import lombok.Data;

@Data
public class DepositeDto implements Serializable {

  private String id;

  private String codeEmployee;

  private String codeCompany;

  private DepositeTypeEnum giftType;

  private BigDecimal prize;

  private boolean active;

  private Date startDeposite;

  private Date endDeposite;

  private Long version;
}
