package com.wedoogift.population.validator;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import com.wedoogift.core.constants.ErrorConstants;
import com.wedoogift.core.enumeration.ActionEnum;
import com.wedoogift.population.api.dto.EmployeeDto;
import com.wedoogift.population.entity.Employee;

@Service
public class EmployeeValidator extends AbstractPopulationValidator<Employee, EmployeeDto> {

  @Override
  protected void validateDto(EmployeeDto dto, Employee existing, ActionEnum action, Map<String, String> validationErrors) {

    if (StringUtils.isEmpty(dto.getCode())) {
      validationErrors.put(ErrorConstants.CODE_REQUIRED_KEY, ErrorConstants.CODE_REQUIRED_LIB);
    }

    if (StringUtils.isEmpty(dto.getMatricule())) {
      validationErrors.put(ErrorConstants.MATRICULE_REQUIRED_KEY, ErrorConstants.MATRICULE_REQUIRED_LIB);
    }

    if (StringUtils.isEmpty(dto.getCompany())) {
      validationErrors.put(ErrorConstants.COMPANY_REQUIRED_KEY, ErrorConstants.COMPANY_REQUIRED_LIB);
    }

    if (StringUtils.isEmpty(dto.getName())) {
      validationErrors.put(ErrorConstants.NAME_REQUIRED_KEY, ErrorConstants.NAME_REQUIRED_LIB);
    }

    if (StringUtils.isEmpty(dto.getSurname())) {
      validationErrors.put(ErrorConstants.SURNAME_REQUIRED_KEY, ErrorConstants.SURNAME_REQUIRED_LIB);
    }
  }
}
