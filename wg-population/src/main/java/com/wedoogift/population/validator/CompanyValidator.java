package com.wedoogift.population.validator;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import com.wedoogift.core.constants.ErrorConstants;
import com.wedoogift.core.enumeration.ActionEnum;
import com.wedoogift.population.api.dto.CompanyDto;
import com.wedoogift.population.entity.Company;

@Service
public class CompanyValidator extends AbstractPopulationValidator<Company, CompanyDto> {
  @Override
  protected void validateDto(CompanyDto dto, Company exiting, ActionEnum action, Map<String, String> validationErrors) {
    if (StringUtils.isEmpty(dto.getCode())) {
      validationErrors.put(ErrorConstants.CODE_REQUIRED_KEY, ErrorConstants.CODE_REQUIRED_LIB);
    }

    if (StringUtils.isEmpty(dto.getLibelle())) {
      validationErrors.put(ErrorConstants.LIBELLE_REQUIRED_KEY, ErrorConstants.LIBELLE_REQUIRED_LIB);
    }
  }
}
