package com.wedoogift.population.validator;

import java.util.HashMap;
import java.util.Map;

import org.springframework.util.CollectionUtils;

import com.wedoogift.core.constants.ErrorConstants;
import com.wedoogift.core.enumeration.ActionEnum;
import com.wedoogift.core.exception.WedoogiftValidationException;
import com.wedoogift.population.api.dto.AbstractPopulationDto;
import com.wedoogift.population.entity.AbstractPopulationEntity;

public abstract class AbstractPopulationValidator<E extends AbstractPopulationEntity, D extends AbstractPopulationDto> {

  public void validate(D dto, E existing, ActionEnum action) {
    Map<String, String> validationErrors = new HashMap<>();
    validateDto(dto, existing, action, validationErrors);

    if (!CollectionUtils.isEmpty(validationErrors)) {
      throw new WedoogiftValidationException(dto.getClass(), validationErrors);
    }

    validateExisting(dto, existing != null, action, validationErrors);

    if (!CollectionUtils.isEmpty(validationErrors)) {
      throw new WedoogiftValidationException(dto.getClass(), validationErrors);
    }
  }

  protected abstract void validateDto(D dto, E existing, ActionEnum action, Map<String, String> validationErrors);

  protected void validateExisting(D dto, boolean existing, ActionEnum action, Map<String, String> validationErrors) {
    if (ActionEnum.CREATE.equals(action) && existing) {
      validationErrors.put(ErrorConstants.ALREADY_EXIST_ENTITY_KEY, String.format(ErrorConstants.ALREADY_EXIST_ENTITY_LIB, dto.getCode()));
    }

    if (ActionEnum.UPDATE.equals(action) && !existing) {
      validationErrors.put(ErrorConstants.NOT_EXIST_ENTITY_KEY, String.format(ErrorConstants.NOT_EXIST_ENTITY_LIB, dto.getCode()));
    }
  }
}
