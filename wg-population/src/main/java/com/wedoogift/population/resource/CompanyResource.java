package com.wedoogift.population.resource;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wedoogift.core.utils.UrlUtils;
import com.wedoogift.population.api.dto.CompanyDto;
import com.wedoogift.population.service.CompanyService;

@RestController
@RequestMapping(value = UrlUtils.COMPANY_URL)
public class CompanyResource extends AbstractPopulationResource<CompanyDto, CompanyService> {

  public CompanyResource(CompanyService service) {
    super(service);
  }
}
