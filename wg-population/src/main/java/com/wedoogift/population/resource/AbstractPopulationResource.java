package com.wedoogift.population.resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.wedoogift.population.api.dto.AbstractPopulationDto;
import com.wedoogift.population.service.AbstractPopulationService;

public abstract class AbstractPopulationResource<D extends AbstractPopulationDto, S extends AbstractPopulationService> {

  protected S service;

  protected AbstractPopulationResource(S service) {
    this.service = service;
  }

  @PostMapping
  public D create(@RequestBody D dto) {
    return (D) service.create(dto);
  }

  @PutMapping
  public D update(@RequestBody D dto) {
    return (D) service.update(dto);
  }

  @GetMapping
  public D findByCode(@RequestParam String code) {
    return (D) service.findByCode(code);
  }
}
