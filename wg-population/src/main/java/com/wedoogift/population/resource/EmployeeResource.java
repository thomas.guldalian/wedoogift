package com.wedoogift.population.resource;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wedoogift.core.utils.UrlUtils;
import com.wedoogift.population.api.dto.EmployeeDto;
import com.wedoogift.population.service.EmployeeService;

@RestController
@RequestMapping(value = UrlUtils.EMPLOYEE_URL)
public class EmployeeResource extends AbstractPopulationResource<EmployeeDto, EmployeeService> {

  public EmployeeResource(EmployeeService service) {
    super(service);
  }
}
