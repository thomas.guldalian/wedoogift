package com.wedoogift.population.dao;

import java.util.Optional;

import org.springframework.data.repository.NoRepositoryBean;

import com.wedoogift.population.entity.AbstractPopulationEntity;
import com.wedoogift.population.repository.AbstractPopulationRepository;

@NoRepositoryBean
public abstract class AbstractPopulationDao<E extends AbstractPopulationEntity, R extends AbstractPopulationRepository<E>> {

  protected R repository;

  protected AbstractPopulationDao(R repository) {
    this.repository = repository;
  }

  public E save(E entity) {
    return repository.save(entity);
  }

  public Optional<E> findById(String code) {
    return repository.findById(code);
  }
}
