package com.wedoogift.population.dao;

import org.springframework.stereotype.Repository;

import com.wedoogift.population.entity.Employee;
import com.wedoogift.population.repository.EmployeeRepository;

@Repository
public class EmployeeDao extends AbstractPopulationDao<Employee, EmployeeRepository> {

  public EmployeeDao(EmployeeRepository repository) {
    super(repository);
  }
}
