package com.wedoogift.population.dao;

import org.springframework.stereotype.Repository;

import com.wedoogift.population.entity.Company;
import com.wedoogift.population.repository.CompanyRepository;

@Repository
public class CompanyDao extends AbstractPopulationDao<Company, CompanyRepository> {

  public CompanyDao(CompanyRepository repository) {
    super(repository);
  }
}
