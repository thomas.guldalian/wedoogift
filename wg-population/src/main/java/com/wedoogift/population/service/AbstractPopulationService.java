package com.wedoogift.population.service;

import com.wedoogift.core.enumeration.ActionEnum;
import com.wedoogift.population.api.dto.AbstractPopulationDto;
import com.wedoogift.population.dao.AbstractPopulationDao;
import com.wedoogift.population.entity.AbstractPopulationEntity;
import com.wedoogift.population.mapper.AbstractPopulationMapper;
import com.wedoogift.population.repository.AbstractPopulationRepository;
import com.wedoogift.population.validator.AbstractPopulationValidator;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public abstract class AbstractPopulationService<E extends AbstractPopulationEntity, D extends AbstractPopulationDto, R extends AbstractPopulationRepository<E>, V extends AbstractPopulationValidator<E, D>, S extends AbstractPopulationDao<E, R>, M extends AbstractPopulationMapper<E, D>> {

  protected V validator;

  protected S dao;

  protected M mapper;

  public D create(D dto) {
    E existing = dao.findById(dto.getCode()).orElse(null);
    validator.validate(dto, existing, ActionEnum.CREATE);
    E entity = mapper.toEntity(dto);
    E save = dao.save(entity);
    return mapper.toDto(save);

  }

  public D update(D dto) {
    E existing = dao.findById(dto.getCode()).orElse(null);
    validator.validate(dto, existing, ActionEnum.UPDATE);
    E entity = mapper.toEntity(dto);
    E save = dao.save(entity);
    return mapper.toDto(save);

  }

  public D findByCode(String code) {
    return mapper.toDto(dao.findById(code).orElse(null));
  }

}
