package com.wedoogift.population.service;

import org.springframework.stereotype.Service;

import com.wedoogift.population.api.dto.CompanyDto;
import com.wedoogift.population.dao.CompanyDao;
import com.wedoogift.population.entity.Company;
import com.wedoogift.population.mapper.CompanyMapper;
import com.wedoogift.population.repository.CompanyRepository;
import com.wedoogift.population.validator.CompanyValidator;

@Service
public class CompanyService extends AbstractPopulationService<Company, CompanyDto, CompanyRepository, CompanyValidator, CompanyDao, CompanyMapper> {

  public CompanyService(CompanyValidator validator, CompanyDao dao, CompanyMapper mapper) {
    super(validator, dao, mapper);
  }
}
