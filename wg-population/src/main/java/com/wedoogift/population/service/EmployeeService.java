package com.wedoogift.population.service;

import org.springframework.stereotype.Service;

import com.wedoogift.population.api.dto.EmployeeDto;
import com.wedoogift.population.dao.EmployeeDao;
import com.wedoogift.population.entity.Employee;
import com.wedoogift.population.mapper.EmployeeMapper;
import com.wedoogift.population.repository.EmployeeRepository;
import com.wedoogift.population.validator.EmployeeValidator;

@Service
public class EmployeeService extends
        AbstractPopulationService<Employee, EmployeeDto, EmployeeRepository, EmployeeValidator, EmployeeDao, EmployeeMapper> {

  public EmployeeService(EmployeeValidator validator, EmployeeDao dao, EmployeeMapper mapper) {
    super(validator, dao, mapper);
  }
}
