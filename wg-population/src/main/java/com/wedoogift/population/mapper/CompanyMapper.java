package com.wedoogift.population.mapper;

import org.mapstruct.Mapper;

import com.wedoogift.population.api.dto.CompanyDto;
import com.wedoogift.population.entity.Company;

@Mapper(componentModel = "spring")
public interface CompanyMapper extends AbstractPopulationMapper<Company, CompanyDto> {
}
