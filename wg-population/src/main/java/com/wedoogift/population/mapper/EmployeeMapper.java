package com.wedoogift.population.mapper;

import org.mapstruct.Mapper;

import com.wedoogift.population.api.dto.EmployeeDto;
import com.wedoogift.population.entity.Employee;

@Mapper(componentModel = "spring")
public interface EmployeeMapper extends AbstractPopulationMapper<Employee, EmployeeDto> {
}
