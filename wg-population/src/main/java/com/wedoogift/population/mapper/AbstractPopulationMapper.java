package com.wedoogift.population.mapper;

import com.wedoogift.population.api.dto.AbstractPopulationDto;
import com.wedoogift.population.entity.AbstractPopulationEntity;

public interface AbstractPopulationMapper<E extends AbstractPopulationEntity, D extends AbstractPopulationDto> {

  D toDto(E entity);

  E toEntity(D dto);
}
