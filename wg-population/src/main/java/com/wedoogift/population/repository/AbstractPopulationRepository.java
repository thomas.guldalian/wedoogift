package com.wedoogift.population.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;

import com.wedoogift.population.entity.AbstractPopulationEntity;

@NoRepositoryBean
public interface AbstractPopulationRepository<E extends AbstractPopulationEntity>extends JpaRepository<E, String>, JpaSpecificationExecutor<E> {
}
