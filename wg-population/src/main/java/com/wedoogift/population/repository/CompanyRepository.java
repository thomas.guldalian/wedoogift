package com.wedoogift.population.repository;

import org.springframework.stereotype.Repository;

import com.wedoogift.population.entity.Company;

@Repository
public interface CompanyRepository extends AbstractPopulationRepository<Company> {
}
