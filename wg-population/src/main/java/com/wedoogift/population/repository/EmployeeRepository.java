package com.wedoogift.population.repository;

import org.springframework.stereotype.Repository;

import com.wedoogift.population.entity.Employee;

@Repository
public interface EmployeeRepository extends AbstractPopulationRepository<Employee> {
}
