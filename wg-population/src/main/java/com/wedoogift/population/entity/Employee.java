package com.wedoogift.population.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;

import org.hibernate.envers.Audited;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;

@Entity
@Table(name = "WG_EMPLOYEE")
@Audited
@EntityListeners(AuditingEntityListener.class)
@Data
public class Employee extends AbstractPopulationEntity {

  @Column(name = "matricule", nullable = false)
  private String matricule;

  @Column(name = "name", nullable = false)
  private String name;

  @Column(name = "surname", nullable = false)
  private String surname;

  @Column(name = "fk_company_code", nullable = false)
  private String company;
}
