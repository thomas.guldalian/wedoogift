package com.wedoogift.population.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;

import org.hibernate.envers.Audited;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;

@Entity
@Table(name = "WG_COMPANY")
@Audited
@EntityListeners(AuditingEntityListener.class)
@Data
public class Company extends AbstractPopulationEntity {

  @Column(name = "libelle", nullable = false)
  private String libelle;
}
