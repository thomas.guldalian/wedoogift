package com.wedoogift.population;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

import com.wedoogift.core.DefaultProfileUtil;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients(basePackages = {"com.wedoogift"})
public class WgPopulationApplication {

  public static void main(String[] args) {
    SpringApplication app = new SpringApplication(WgPopulationApplication.class);
    DefaultProfileUtil.addDefaultProfile(app);
    app.run(args).getEnvironment();
  }
}
