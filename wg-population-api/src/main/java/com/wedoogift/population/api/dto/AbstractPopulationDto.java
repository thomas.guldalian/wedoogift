package com.wedoogift.population.api.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

@Data
public abstract class AbstractPopulationDto implements Serializable {

  private String code;

  private long version;

  private Date createdDate;

  private Date modifiedDate;
}
