package com.wedoogift.population.api.rest;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.wedoogift.core.utils.UrlUtils;
import com.wedoogift.population.api.dto.CompanyDto;
import com.wedoogift.population.api.dto.EmployeeDto;

@FeignClient(value = "wg-population")
public interface WgPopulationApi {

  @GetMapping(value = UrlUtils.COMPANY_URL)
  public CompanyDto findCompanyByCode(@RequestParam String code);

  @GetMapping(value = UrlUtils.EMPLOYEE_URL)
  public EmployeeDto findEmployeeByCode(@RequestParam String code);
}
