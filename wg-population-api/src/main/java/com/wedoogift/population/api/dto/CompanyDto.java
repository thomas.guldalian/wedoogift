package com.wedoogift.population.api.dto;

import lombok.Data;

@Data
public class CompanyDto extends AbstractPopulationDto {

  private String libelle;
}
