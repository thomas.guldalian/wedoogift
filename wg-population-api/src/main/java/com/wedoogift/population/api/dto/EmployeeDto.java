package com.wedoogift.population.api.dto;

import lombok.Data;

@Data
public class EmployeeDto extends AbstractPopulationDto {

  private String matricule;

  private String name;

  private String surname;

  private String company;
}
