# Projet GTA SNEF

Bienvenue sur le repository du projet Wedoogift !
Vous trouverez ci-dessous la documentation d'installation ainsi que les procédures pour exécuter les 2 fonctionnalités
demandées.

## Environnement de développement

- Docker desktop
- IntelliJ
- Java (JAVA_HOME) version [11.0.10](https://www.oracle.com/fr/java/technologies/javase-jdk11-downloads.html)
- Maven (MVN_HOME) [3.6.3](https://maven.apache.org/download.cgi)
- Client PostgresSQL ([Dbeaver](https://dbeaver.io/download/))
- [Postman](https://www.postman.com/)
- Terminal sous Windows ([Cmder](https://cmder.net/))
- Git

## Installation

### Docker

* avec un terminal de commande (ex: cmder), se mettre dans le repository docker.
* lancer la commande : ``docker-compose -p wedoogift up -d`` (p- wedoogift nécessaire si vous avez déjà une
  configuration docker existante)

### Configuration client PostgresSQL

Ajouter une nouvelle connexion Postgres :

- Host : localhost
- Database : postgres
- Port : 5432
- User name : postgres
- Password : postgres (password défini dans docker-compose)

Avec ce user il faut jouer les scripts suivants permettant de créer le user (utilisé par l'application) Wedoogift ainsi
que sa base de données:

- delivery/postgres/1_init_postgres.sql

### Configuration DynamoDB

Dans docker, j'ai configuré le container dynamoDB et dynamoDB Admin, une IHM permettant de visualiser les données.
l'accès à cette interface se fait avec l'url : http://localhost:8001/

Avec un terminal de commande, il faut initialiser les 3 différentes tables.
Il faut avoir aws-cli d'installé :

- npm install -g aws-cli

Ces fichiers sont situés ici : delivery/aws

````
- aws dynamodb create-table --cli-input-json file://transaction.json --endpoint-url http://localhost:8000
- aws dynamodb create-table --cli-input-json file://deposite.json --endpoint-url http://localhost:8000
- aws dynamodb create-table --cli-input-json file://companyBalance.json --endpoint-url http://localhost:8000
`````

### autre configuration

* Configurer les variables d'environnement Java/Maven

### Intellij

* Installer Intellij
* Importer le projet
* Activer l'annotation processor
* Clean install le projet

### Initialisation des données population

A chaque lancement du service population, l'ensemble des bases Employee et Company est recréé.
L'initialisation des données se fait par le premier dossier du postman situé ici : **
delivery/postman/population.postman_collection.json**

### fonctionnalité demandé pour le test technique

Les 2 fonctionnalités demandées pour le test technique sont déclarées dans le deuxième dossier du postman situé ici : **
delivery/postman/wedoogift.postman_collection.json**
