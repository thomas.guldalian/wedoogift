package com.wedoogift.core;

import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.SpringApplication;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class DefaultProfileUtil {
  private static final String SPRING_PROFILE_DEFAULT = "spring.profiles.default";

  public static void addDefaultProfile(SpringApplication app) {
    Map<String, Object> defProperties = new HashMap<>();
    defProperties.put(SPRING_PROFILE_DEFAULT, "dev");
    app.setDefaultProperties(defProperties);
  }
}