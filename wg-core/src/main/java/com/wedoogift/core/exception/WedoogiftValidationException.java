package com.wedoogift.core.exception;

import java.io.Serializable;
import java.util.Map;

import com.wedoogift.core.constants.ExceptionConstants;

import lombok.Data;

@Data
public class WedoogiftValidationException extends RuntimeException implements Serializable {

  private final String entity;

  private final Map<String, String> validationErrors;

  public WedoogiftValidationException(Class<?> objetClass, Map<String, String> validationErrors) {
    super(ExceptionConstants.ERREUR_VALIDATION + " : " + objetClass.getSimpleName());
    this.entity = objetClass.getClass().getSimpleName();
    this.validationErrors = validationErrors;
  }
}