package com.wedoogift.core.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class UrlUtils {

  public static final String COMPANY_URL = "api/v1/company";

  public static final String EMPLOYEE_URL = "api/v1/employee";

  public static final String TRANSACTION_URL = "api/v1/transaction";
}
