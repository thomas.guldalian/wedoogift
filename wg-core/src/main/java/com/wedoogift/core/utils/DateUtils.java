package com.wedoogift.core.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class DateUtils {

  private static final DateFormat dateFormat;

  public static final String DYNAMO_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

  static {
    dateFormat = new SimpleDateFormat(DYNAMO_DATE_FORMAT);
  }

  public static String formatDate(Date date) {
    return (date != null) ? dateFormat.format(date) : null;
  }

  public static Date getDateFromLocal(LocalDate local) {
    if (null == local) {
      return null;
    }
    ZoneId defaultZoneId = ZoneId.systemDefault();
    LocalTime localTime = LocalTime.of(0, 0);
    LocalDateTime localDateTime = LocalDateTime.of(local, localTime);
    ZonedDateTime zonedDateTime = ZonedDateTime.of(localDateTime, defaultZoneId);
    return Date.from(zonedDateTime.toInstant());
  }

  public static Calendar getCalendarZonedFromYearMonthDay(Integer year, Integer month, Integer day) {
    Calendar calendar = Calendar.getInstance();
    if (null != year && null != day) {
      if (null == month) {
        calendar.set(year, 0, day);
      } else {
        calendar.set(year, month - 1, day);
      }
    }

    resetTime(calendar);
    return calendar;
  }

  public static Date getLastDayOfMonth(Integer month, Integer year) {
    Calendar c = getCalendarInstance();
    resetTime(c);
    if (null != year) {
      c.set(Calendar.YEAR, year);
    }
    if (null != month) {
      c.set(Calendar.MONTH, month - 1);
    }
    c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
    return c.getTime();
  }

  public static Date addUnitsToDate(int type, int nb, Date date) {
    Calendar cal = getCalendarInstance();
    if (null != date) {
      cal.setTime(date);
    }
    cal.add(type, nb);
    return cal.getTime();
  }

  public static Calendar getCalendar(Date date) {
    Calendar cal = getCalendarInstance();
    if (null != date) {
      cal.setTime(date);
    }
    resetTime(cal);
    return cal;
  }

  private static Calendar getCalendarInstance() {
    Calendar cal = Calendar.getInstance();
    cal.setFirstDayOfWeek(Calendar.MONDAY);
    cal.set(Calendar.HOUR_OF_DAY, cal.getActualMinimum(Calendar.HOUR_OF_DAY));
    cal.set(Calendar.MINUTE, cal.getActualMinimum(Calendar.MINUTE));
    cal.set(Calendar.SECOND, cal.getActualMinimum(Calendar.SECOND));
    cal.set(Calendar.MILLISECOND, cal.getActualMinimum(Calendar.MILLISECOND));
    return cal;
  }

  private static void resetTime(Calendar cal) {
    if (null != cal) {
      cal.set(Calendar.HOUR_OF_DAY, cal.getActualMinimum(Calendar.HOUR_OF_DAY));
      cal.set(Calendar.MINUTE, cal.getActualMinimum(Calendar.MINUTE));
      cal.set(Calendar.SECOND, cal.getActualMinimum(Calendar.SECOND));
      cal.set(Calendar.MILLISECOND, cal.getActualMinimum(Calendar.MILLISECOND));
    }
  }
}
