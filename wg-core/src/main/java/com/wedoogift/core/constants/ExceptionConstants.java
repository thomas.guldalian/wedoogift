package com.wedoogift.core.constants;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ExceptionConstants {

  public static final String ERREUR_VALIDATION = "Validation error of object";
}
