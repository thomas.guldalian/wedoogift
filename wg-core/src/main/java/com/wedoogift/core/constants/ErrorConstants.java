package com.wedoogift.core.constants;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ErrorConstants {

  public static final String UNKNOW_ENTITY_KEY = "UNKNOW_ENTITY";

  public static final String UNKNOW_ENTITY_LIB = "%s unknow";

  public static final String ALREADY_EXIST_ENTITY_KEY = "ALREADY_EXIST_ENTITY";

  public static final String ALREADY_EXIST_ENTITY_LIB = "Entity already exist with code : %s";

  public static final String NOT_EXIST_ENTITY_KEY = "NOT_EXIST_ENTITY";

  public static final String NOT_EXIST_ENTITY_LIB = "Entity to update not exist with code : %s";

  public static final String CODE_REQUIRED_KEY = "CODE_REQUIRED";

  public static final String CODE_REQUIRED_LIB = "code required";

  public static final String LIBELLE_REQUIRED_KEY = "LIBELLE_REQUIRED";

  public static final String LIBELLE_REQUIRED_LIB = "libelle required";

  public static final String MATRICULE_REQUIRED_KEY = "MATRICULE_REQUIRED";

  public static final String MATRICULE_REQUIRED_LIB = "matricule required";

  public static final String COMPANY_REQUIRED_KEY = "COMPANY_REQUIRED";

  public static final String COMPANY_REQUIRED_LIB = "company required";

  public static final String NAME_REQUIRED_KEY = "NAME_REQUIRED";

  public static final String NAME_REQUIRED_LIB = "name required";

  public static final String SURNAME_REQUIRED_KEY = "SURNAME_REQUIRED";

  public static final String SURNAME_REQUIRED_LIB = "surname required";

  public static final String NO_MATCH_COMPANY_EMPLOYEE_KEY = "NO_MATCH_COMPANY_EMPLOYEE";

  public static final String NO_MATCH_COMPANY_EMPLOYEE_LIB = "this employee is not on the company: %s";

  public static final String NOT_ENOUGH_BALANCE_KEY = "NOT_ENOUGH_BALANCE";

  public static final String NOT_ENOUGH_BALANCE_LIB = "compnay haven't enough balance, current: %s";

  public static final String INVALID_ENUM_ERROR = "invalid enum value of %s. values: %s";
}
