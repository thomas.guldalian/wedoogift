package com.wedoogift.core.error;

import java.util.Date;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.lang.Nullable;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class ErrorsWrapperDto {

  private HttpStatus status;

  private String message;

  private String entity;

  private Map<String, String> data;

  private Date date;

  private String path;

  private String requestMethod;

  public ErrorsWrapperDto(HttpStatus status, @Nullable String message, String path) {
    this(status, message, path, null, null, null);
  }

  public ErrorsWrapperDto(HttpStatus status, @Nullable String message, @Nullable String path, String requestMethod,
          @Nullable Map<String, String> data, String entity) {
    this.status = status;
    this.message = message;
    this.path = path;
    this.entity = entity;
    this.data = data;
    this.date = new Date();
    this.requestMethod = requestMethod;
  }

  public ErrorsWrapperDto(HttpStatus status, String message, String path, String method) {
    this(status, message, path, null, null, method);
  }

  public HttpStatus getStatus() {
    return status;
  }

  @Nullable
  public String getMessage() {
    return message;
  }

  @Nullable
  public Map<String, String> getData() {
    return data;
  }

  @Nullable
  public Date getDate() {
    return date;
  }

  @Nullable
  public String getPath() {
    return path;
  }
}
