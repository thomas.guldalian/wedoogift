package com.wedoogift.core.error;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.wedoogift.core.exception.WedoogiftValidationException;

import lombok.extern.slf4j.Slf4j;

@RestControllerAdvice
@Slf4j
public class ExceptionTranslator {

  public static final String XWG_ERROR = "X-WG-ERROR";

  private static final String REQUEST_RAISED_AN_EXCEPTION = "Request raised an Exception";

  @ExceptionHandler(WedoogiftValidationException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public ResponseEntity<ErrorsWrapperDto> handleWedooGiftValidationException(WedoogiftValidationException ex, HttpServletRequest request,
          HttpServletResponse response) {
    final Map<String, String> validationResults = ex.getValidationErrors();

    logMapResults("ValidationException", validationResults);
    log.info(REQUEST_RAISED_AN_EXCEPTION, ex);
    HttpHeaders headers = new HttpHeaders();
    headers.set(XWG_ERROR, "1");

    return new ResponseEntity<>(getErrorsWrapperDto(ex, request, HttpStatus.BAD_REQUEST, validationResults, ex.getEntity()), headers,
            HttpStatus.BAD_REQUEST);
  }

  private ErrorsWrapperDto getErrorsWrapperDto(Exception ex, HttpServletRequest request, HttpStatus httpStatus, Map<String, String> mapErrors,
          String entity) {
    return new ErrorsWrapperDto(httpStatus, ex.getMessage(), request.getRequestURI(), request.getMethod(), mapErrors, entity);
  }

  private void logMapResults(String exceptionType, Map<String, String> map) {
    if (log.isDebugEnabled() && !CollectionUtils.isEmpty(map)) {
      StringBuilder sb = new StringBuilder();
      sb.append("Request raised a " + exceptionType + " exception. ");
      int nbErrors = map.size();
      sb.append(nbErrors).append(" Error");
      if (nbErrors > 1) {
        sb.append("s");
      }
      sb.append(" : ");

      for (Map.Entry<String, String> entry : map.entrySet()) {
        sb.append(entry.getKey()).append(" : ").append(entry.getValue());
      }
      log.debug(sb.toString());
    }
  }
}
