create user wedoogift;
alter user wedoogift with encrypted password 'wedoogift';
create database wedoogift;
grant all privileges on database wedoogift to wedoogift;